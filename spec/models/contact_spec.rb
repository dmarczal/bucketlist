require 'rails_helper'

RSpec.describe Contact, type: :model do

  before(:each) do
    @contact = create(:contact)
  end

  it "should be valid when all fields are filled with valid data" do
    expect(@contact).to be_valid
  end

  it "should not be valid when name is blank" do 
    @contact.name=" "
    expect(@contact).to_not be_valid
  end


  it "should not be valid when message is blank" do
    @contact.message=" "
    expect(@contact).to_not be_valid
  end

  it "should accept only valid email" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]

    valid_addresses.each do |valid_address|
      @contact.email = valid_address
      expect(@contact).to be_valid, "#{valid_address.inspect} should be valid"
    end
  end

  it "should not accept only invalid email" do
    valid_addresses = %w[userexample.com USER-foo.COM A_US-ER@foo
                         first.last@foo.jp.123 alice+bobbaz.cn]

    valid_addresses.each do |valid_address|
      @contact.email = valid_address
      expect(@contact).to_not be_valid, "#{valid_address.inspect} should be invalid"
    end
  end

end

