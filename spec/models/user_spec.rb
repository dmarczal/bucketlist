require 'rails_helper'

RSpec.describe User, type: :model do

  it "should be valid when the name is present" do
    user = create(:user)
    expect(user).to be_valid
  end

  it "should be invalid when the name is blank" do
    user = create(:user)
    user.name = ""
    expect(user).to be_invalid
  end

end
