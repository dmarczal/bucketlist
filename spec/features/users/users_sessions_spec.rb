require 'rails_helper'

describe "Users:Sessions", type: :feature do

  it "displays the user's perfil links after successful login" do
      user = FactoryGirl.create(:user)

      visit new_user_session_path
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: 'password'

      click_on 'Entrar'

      expect(page.current_path).to eq root_path
      expect(page).to have_selector('a', text: 'Meu perfil')
      expect(page).to have_selector('div.alert.alert-info',
                                    text: 'Logado com sucesso.')
    end

end
