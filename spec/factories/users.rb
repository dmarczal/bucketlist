FactoryGirl.define do
  factory :user do
    name 'Fulano'
    sequence :email { |n| "user#{n}@tsi.gp.utfpr.edu.br" }
    password 'password'
    password_confirmation 'password'
  end
end
