## Correções:

$ git checkout master
$ git checkout -b authentication

- Adicionar devise.pt-Br.yml
https://github.com/plataformatec/devise/wiki/I18n

- config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

- $ rails generate devise MODEL

- $ rails db:migrate

- Tradução do user

- Editar navbar

- Layout das sessions

- css e js

- arrumar o css do login

- devise views

- testes

- database cleaner

- setup email

- commit e deploy
