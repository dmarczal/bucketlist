Rails.application.routes.draw do
  devise_for :admins
  root to: 'home#index'

  devise_for :users
  resources :contacts, only: [:create]

  authenticate :user do
    namespace :users do
      root to: 'dashboard#index'
      get '/about', to: 'dashboard#about'
      get '/tasks', to: 'tasks#about'
    end
  end

  authenticate :admin do
    namespace :admins do
      root to: 'dashboard#index'
      get '/about', to: 'dashboard#about'
    end
  end

end
