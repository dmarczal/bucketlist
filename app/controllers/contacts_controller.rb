class ContactsController < ApplicationController
  def create
    @contact = Contact.new(contact_parms)

    if @contact.save
      flash[:success] = "Mensagem enviada com sucesso!"
      #redirect_to root_path
      @contact = Contact.new
    else
      flash[:error] = "Existem dados incorretos! Por favor verifique."
      #render "home/index"
    end

  end

  private
  def contact_parms
    params.require(:contact).permit(:name, :email, :message)
  end
end
